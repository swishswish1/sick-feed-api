import { Injectable } from '@nestjs/common';
import { Database, Event as Events } from '@sick/typeorm';

@Injectable()
export class FeedService {
	constructor(private database: Database) {}

	public async getEvents(
		earliest: string,
		latest: string,
		from?: number,
		size?: number,
		povIds?: string[],
		resources?: string[],
		operations?: string[],
		entityTypes?: string[]
	) {
		const query = this.database
			.getConnection()
			.getRepository(Events)
			.createQueryBuilder('events');

		query.where('events.timestamp BETWEEN :earliest AND :latest', { earliest, latest });

		if (povIds && povIds.length) {
			query.andWhere('events.povId IN (:...povIds)', { povIds });
		}

		if (resources && resources.length) {
			query.andWhere('events.resource IN (:...resources)', { resources });
		}

		if (operations && operations.length) {
			query.andWhere('events.operation IN (:...operations)', { operations });
		}

		if (entityTypes && entityTypes.length) {
			query.andWhere('events.entityType IN (:...entityTypes) OR events.parentEntityType IN (:...entityTypes)', { entityTypes });
		}

		if (!!from || from === 0) {
			query.skip(from);
		}

		if (!!size || size === 0) {
			query.take(size);
		}

		const events = await query.getMany();

		return events;
	}
}
