import { Module } from '@nestjs/common';
import { ConfigModule } from '@sick/config';
import { LoggerModule } from '@sick/logger';
import { DatabaseModule } from '@sick/typeorm';
import { FeedController } from './feed.controller';
import { FeedService } from './feed.service';

@Module({
	imports: [ConfigModule, DatabaseModule, LoggerModule],
	controllers: [FeedController],
	providers: [FeedService],
})
export class FeedModule {}
