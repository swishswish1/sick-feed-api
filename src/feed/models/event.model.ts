import { ApiProperty } from '@nestjs/swagger';

export enum EventOperation {
	Create = 'create',
	Update = 'update',
	Delete = 'delete',
}

export enum EventResource {
	Entity = 'entity',
	Field = 'field',
	Relation = 'relation',
	Insight = 'insight',
	File = 'file',
	Reference = 'reference',
}

export class Event {
	id: string;

	insertionTime: string;

	@ApiProperty({ required: true })
	timestamp: string;

	@ApiProperty({ required: true })
	povId: string;

	@ApiProperty({ required: true, enum: EventResource })
	resource: EventResource;

	@ApiProperty({ required: true, enum: EventOperation })
	operation: EventOperation;

	@ApiProperty({ required: true })
	parentEntityType: string;

	@ApiProperty({ required: true })
	entityType: string;

	@ApiProperty({ required: true })
	entityId: string;

	@ApiProperty({ required: true })
	resourceId: string;
}
