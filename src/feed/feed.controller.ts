import { Controller, Get, Query } from '@nestjs/common';
import { ApiHeader, ApiOkResponse, ApiQuery, ApiTags } from '@nestjs/swagger';
import { FeedService } from './feed.service';
import { Event, EventOperation, EventResource } from './models/event.model';

@ApiTags('Feed')
@ApiHeader({
	name: 'System-Name',
	description: 'The system that makes the request',
	required: true,
})
@ApiHeader({
	name: 'Client-Name',
	description: 'The client that make the request',
	required: true,
})
@ApiHeader({
	name: 'Authorization',
	description: 'The api key of the system or the client',
	required: true,
})
@Controller('feed')
export class FeedController {
	constructor(private readonly feedService: FeedService) {}

	@Get()
	@ApiOkResponse({ type: Event, isArray: true })
	@ApiQuery({ name: 'from', required: false })
	@ApiQuery({ name: 'size', required: false })
	@ApiQuery({ name: 'povId', required: false })
	@ApiQuery({ name: 'resource', required: false, enum: EventResource })
	@ApiQuery({ name: 'operation', required: false, enum: EventOperation })
	@ApiQuery({ name: 'entityType', required: false })
	public async getEvents(
		@Query('earliest') earliest: string,
		@Query('latest') latest: string,
		@Query('from') from?: number,
		@Query('size') size?: number,
		@Query('povId') povId?: string[],
		@Query('resource') resource?: string[],
		@Query('operation') operation?: string[],
		@Query('entityType') entityType?: string[]
	) {
		[povId, resource, operation, entityType] = [
			(Array.isArray(povId) ? povId : [povId]).filter(item => !!item),
			(Array.isArray(resource) ? resource : [resource]).filter(item => !!item),
			(Array.isArray(operation) ? operation : [operation]).filter(item => !!item),
			(Array.isArray(entityType) ? entityType : [entityType]).filter(item => !!item),
		];
		return await this.feedService.getEvents(earliest, latest, from, size, povId, resource, operation, entityType);
	}
}
