import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { ConfigModule } from '@sick/config';
import { LoggerModule } from '@sick/logger';
import { DatabaseModule } from '@sick/typeorm';
import { FeedModule } from './feed/feed.module';

@Module({
	imports: [DatabaseModule, ConfigModule, LoggerModule, TerminusModule, FeedModule],
	controllers: [],
	providers: [],
})
export class AppModule {}
