import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Config } from '@sick/config';
import { ITransporterOptions, Logger } from '@sick/logger';
import { RequestStorage } from '@sick/request-storage';
import { Database } from '@sick/typeorm';
import * as pkg from '../package.json';
import { AppModule } from './app.module';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);

	app.use(RequestStorage.middleware);

	const config = app.get(Config);
	await config.init(pkg.name);

	const logger = app.get(Logger);
	logger.init(config.get<ITransporterOptions[]>('LOGGER.TRANSPORTERS'));

	// app.useLogger(logger); // TODO: Check the useLogger function

	const swaggerOptions = new DocumentBuilder()
		.setTitle(pkg.name)
		.setDescription(pkg.description)
		.setVersion(pkg.version)
		.build();

	const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
	SwaggerModule.setup('swagger', app, swaggerDocument);

	await app
		.get(Database)
		.connect(
			config.get<string>('MSSQL.HOST'),
			config.get<string>('MSSQL.USERNAME'),
			config.get<string>('MSSQL.PASSWORD'),
			config.get<string>('MSSQL.DATABASE')
		);

	await app.listen(3000);
	logger.info(`Application ${pkg.name} is up on ${await app.getUrl()}`);
}

bootstrap();
